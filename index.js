const test1 = require('./test/testProblem1');
const test2 = require('./test/testProblem2');
const test3 = require('./test/testProblem3');
const test4 = require('./test/testProblem4');
const test5 = require('./test/testProblem5');
const test6 = require('./test/testProblem6');

test1();
test2();
test3();
test4();
test5();
test6();
const inventory = require('../inventory.json');
const problem2 = require('../problems/problem2');

let test2 = () => {
    const result = problem2(inventory);
    console.log(`Last car is a ${result['car_make']} ${result['car_model']}`);
}

module.exports = test2;

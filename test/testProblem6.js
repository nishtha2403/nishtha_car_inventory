const inventory = require('../inventory.json');
const problem6 = require('../problems/problem6');

let test6 = () => {
    const result = problem6(inventory);
    console.log(result);
}

module.exports = test6;

const inventory = require('../inventory.json');
const problem1 = require('../problems/problem1');

let test1 = () => {
    const result = problem1(inventory);
    console.log(`Car 33 is a ${result['car_year']} ${result['car_make']} ${result['car_model']}`);
}

module.exports = test1;
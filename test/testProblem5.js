const inventory = require('../inventory.json');
const problem5 = require('../problems/problem5');
const problem4 = require('../problems/problem4');

let test5 = () => {
    const result4 = problem4(inventory);
    const result = problem5(result4);
    console.log(result.length);
}

module.exports = test5;
const inventory = require('../inventory.json');
const problem3 = require('../problems/problem3');

let test3 = () => {
    const result = problem3(inventory);
    console.log(result);
}

module.exports = test3;
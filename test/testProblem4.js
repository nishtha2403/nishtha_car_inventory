const inventory = require('../inventory.json');
const problem4 = require('../problems/problem4');

let test4 = () => {
    const result = problem4(inventory);
    console.log(result);
}

module.exports = test4;

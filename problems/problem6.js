//Only BMW and Audi
let problem6 = (inventory) => {
    let BMWAndAudi = [];

    for(let car of inventory){
        if(car['car_make'] == 'BMW' || car['car_make'] == 'Audi'){
            BMWAndAudi.push(car);
        }
    }

    let BMWAndAudiInStringFormat = JSON.stringify(BMWAndAudi);
    
    return BMWAndAudiInStringFormat;
}

module.exports = problem6;
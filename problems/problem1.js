//To find car with an id 33
let problem1 = (inventory) => {

    let carData = {};

    for(let car of inventory){
        if(car.id == 33){
            carData = car;
            break;
        }
    }

    return carData;
}

module.exports = problem1;
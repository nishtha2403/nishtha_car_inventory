//All the years 
let problem4 = (inventory) => {
    let allYears = [];
    for(let car of inventory){
        allYears.push(car['car_year']);
    }
    
    return allYears;
}

module.exports = problem4;
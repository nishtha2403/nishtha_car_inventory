//Cars older than 2000
let problem5 = (allYears) => {
    let olderThan2000 = [];
    for(let year of allYears){
        if(year > 2000){
            olderThan2000.push(year);
        }
    }
    
    return olderThan2000;
}

module.exports = problem5;
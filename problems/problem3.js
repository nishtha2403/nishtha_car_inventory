//Sort all the car model names
let problem3 = (inventory) => {
    let carModelNamesArr = [];
    for(let car of inventory){
        carModelNamesArr.push(car['car_model']);
    }
    carModelNamesArr.sort();
    
    return carModelNamesArr;
}

module.exports = problem3;